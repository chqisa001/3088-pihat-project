It’s very common for people to setup Pis as home servers/storage devices/media servers.
With rolling blackouts it can be damaging to the Pi’s SDcard to have the power just cut
randomly and repeatedly. This microHAT will keep your Pi alive for long enough to shut it
down properly (or if your battery is big enough to survive the blackout). To help you decide
when to shut the Pi down it would obviously be helpful to know how full your battery is (how
much power you’ve already used) so the HAT will also have a Voltmeter (or even better
would be a current shunt) that will lead to the signal line between 0-3.3V (In a larger design
this value can be digitised or displayed on an output). Similarly some status LEDs showing
eg Battery Voltage above 80%V to show ‘full’, or just an indicator showing “using battery
power / not” or an indicator warning of low voltage.

These are the different scenarios that ia UPS would be used.
1. As a backup power supply
● The UPS should continuously provide power supply to the microcontroller during
line sags.
● The UPS should restart the equipment after prolonged power shortage.
● The UPS should provide ample time for the student to shut down the computer
2. As an electrical component(e.g rectifier)
● The UPS should reduce noise from the power sources.
● The UPS should display the voltage across a line.
● The UPS should display voltage and current that the equipment draws.
3. Protection of the circuit components
● The UPS should provide protection to the components during a surge.
● The UPS should provide protection from the harmonic produced during
transmission.
● The UPS should provide short-circuit protection in any given circuit
